# Interview Questions at CoinGecko

## Quickly navigate them from here

- [Internship (Web/Mobile)](https://gitlab.com/coingecko-dev-test/interview-questions/-/blob/master/positions/internship.md)

- [Software Engineer (L1 - L5)](https://gitlab.com/coingecko-dev-test/interview-questions/-/blob/master/projects/link_shortener.md)

- [DevOps Engineer (All Levels)](https://gitlab.com/coingecko-dev-test/interview-questions/-/blob/master/positions/devops_engineer.md)

- [React Native Engineer](https://gitlab.com/coingecko-dev-test/interview-questions/-/blob/master/positions/react_native_engineer.md)
- [React Native Engineer (Senior)](https://gitlab.com/coingecko-dev-test/interview-questions/-/blob/master/positions/react_native_engineer_senior.md)

- [UI/UX (Junior/Mid)](https://gitlab.com/coingecko-dev-test/interview-questions/-/blob/master/positions/ui_ux_designer_junior_mid.md)
- [UI/UX (Senior)](https://gitlab.com/coingecko-dev-test/interview-questions/-/blob/master/positions/ui_ux_designer_senior.md)

- Product Manager
- QA Engineer

## Projects
- [Crypto Dashboard (Internship)](https://gitlab.com/coingecko-dev-test/interview-questions/-/blob/master/projects/crypto_dashboard_intern.md)
- [Crypto Dashboard (Junior)](https://gitlab.com/coingecko-dev-test/interview-questions/-/blob/master/projects/crypto_dashboard_junior_mid.md)
- [Crypto Cart (Junior/Mid)](https://gitlab.com/coingecko-dev-test/interview-questions/-/blob/master/projects/crypto_cart_mid.md)
- [Crypto Cart (Standard)](https://gitlab.com/coingecko-dev-test/interview-questions/-/blob/master/projects/crypto_cart.md)
- [Link Shortener](https://gitlab.com/coingecko-dev-test/interview-questions/-/blob/master/projects/link_shortener.md)
