# Mobile Lead Interview Questions
*Please include explanation along with your answers.*

1. The mobile team relies a lot on API from the backend team. How would you best coordinate the requirements to streamline communication issues and ensure mobile team gets the support they need?

1. You have just joined the company as a mobile lead, on the first day on the job, the current mobile team shared with you some of the problems that they are facing :-
    - Our app quality is not good, we keep pushing hotfixes in various releases. Developers some time find it difficult to even trace these issues. This also slows down our iteration speed to ship new features.
    - For every hotfix that we need to push, we had to release a build. while the review process is in place, our app ratings continue to plummet.
    - Everytime before we deploy a release to iOS and Android, we had to manually compile, build, and sign the app for each platform before submitting to the store. 
    - As mobile lead, how would you address these problems and come up with a plan to overcome this? If there is not enough detail here, feel free to frame a scenario and your own assumptions. There should be multiple solutions to these problems with different trade offs considered.

1. In one of the townhall meeting, it was raised by other team members that there is no way to determine the quality of the app. What is the crash rate? Are people satisfied with the app? Are there regressions? These are some common questions that were not tracked. How would you address this and come up with a plan to fulfill this?

1. The design team is proposing a complete rehaul of the mobile app design. All pages have a new design. Redesign is difficult and the developer team is pushing back on this due to legacy code and there are features to release. How would you balance this and push for this redesign to happen?

1. A junior developer plans to break into mid-level and senior engineer over the next 2-3 years. As a mentor, how would you guide him/her to reach the goal? What important aspect should they be focusing on?


# Coding Assessment

[Build a Crypto Shop with Cart](https://gitlab.com/coingecko-dev-test/interview-questions/-/blob/master/projects/crypto_cart.md)

---
# Submission instruction

1. Create a new repo
2. In your own git repo work on the Assessment.
3. Once you are done, share the link and access to your gitlab repo in the job application email.
4. Invite the users in https://gitlab.com/groups/coingecko-dev-test/-/group_members

Note: Do not create Merge Request against this repository, else your submission will be automatically disqualified.
