# React Native Interview Questions
*Please include explanation along with your answers.*

1. What is Arrow function in JavaScript? How it differs from normal function?

1. Should a JS project be using Promise, Async-await, Function Generator, or Callback when making HTTP calls?

1. Git: You are asked to work on a new existing project. When you browse the Git repo historical commits, you noticed many commits of small code changes and short description. Provide 3 advice you can give to the existing team to improve their Git practice?

1. When reading an existing React Native codebase, you noticed that String variables are getting passed down to various classes and files finally leading to the View Component to be displayed. It also appears that this is how data is being presented in the codebase for the users. Is this approach alright or can you suggest ways to improve on this? Why?

# Coding Assessment

[Build a Crypto Shop with Cart](https://gitlab.com/coingecko-dev-test/interview-questions/-/blob/master/projects/crypto_cart_mid.md)

---
# Submission instruction

1. Create a new repo
2. In your own git repo work on the Assessment.
3. Once you are done, share the link and access to your gitlab repo in the job application email.
4. Invite the users in https://gitlab.com/groups/coingecko-dev-test/-/group_members and grant Reporter role

Note: Do not create Merge Request against this repository, else your submission will be automatically disqualified.
