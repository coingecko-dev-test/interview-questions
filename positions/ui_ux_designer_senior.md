# Senior Interview Questions

# Part 1 - General Design Questions

1. You joined an existing product team. You find yourself designing mockups and handing over to the engineering team to implement. After a while, you noticed a problem in which the product is deployed, but it did not seem to follow the mockups. Margins are incorrect, padding are incorrect, and wrong color code is being used. In some extreme case, buttons may even look different. 

What could be the reason for this issue and walk through how your approach to narrowing down the problem and solving it.

1. You are part of a project to redesign the entire app. What would be the possible challenges of a redesign, and how would you convey your concern to the project lead; and your approach to ensure a successful redesign.

1. As a designer in a product team, do you consider yourself a team player? How do you collaborate with various different parties?

1. In CSS, you want to style an element. Do you create an ID or Class to style the element? Which is the best way to do it? What if you have 100 more elements to style?

1. It is better to use flex box to position elements? Yes or No? Why?

# Part 2 - Pitch a Design

Traders in the crypto space are always looking for charts or data to help make decisions when a new opportunity arises - be it a newly launched projects or regularly checking their favorite coins. One of that product that is often used in the industry is Poocoin.

For this task, prepare a UI/UX case study pitch deck of a product on improvements that should be made to https://poocoin.app/


