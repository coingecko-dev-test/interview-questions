# Senior Interview Questions

# Part 1 - General Design Questions

1. You have created a mockup to be handed to the engineering team to be implemented. Upon deployment to the production environment, you noticed that the design is not like what you have mocked up. What do you think could be the problem? How do you ensure that the risk of this happening is as low as possible.

1. As a designer in a product team, do you consider yourself a team player? How do you collaborate with various different parties?

1. In CSS, you want to style an element. Do you create an ID or Class to style the element? Which is the best way to do it? What if you have 100 more elements to style?

1. It is better to use flex box to position elements? Yes or No? Why?

# Part 2 - Pitch a Design

Traders in the crypto space are always looking for charts or data to help make decisions when a new opportunity arises - be it a newly launched projects or regularly checking their favorite coins. One of that product that is often used in the industry is Poocoin.

For this task, prepare a UI/UX case study pitch deck of a product on improvements that should be made to https://poocoin.app/


