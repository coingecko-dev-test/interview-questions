# Junior Interview Questions

# Part 1 - General Software Questions

*Please include explanation along with your answers.*

```
TickerMinutely.where(coin_id: “bitcoin”).where(“timestamp > ?”, 3.months.ago)
```
1. Our application needs to plot a bitcoin price chart over 3 months. One of the developers used the following query to obtain the data needed to plot the chart.
After a period of time, our support team is getting complaints from our users that the chart is taking too long to load.
Suggest at least 4 possible ways to improve the performance and fix this bottleneck.

1. You are a web developer working on a web application. Customer support approached you and escalated a problem that users are currently facing. The user visited the page at http://this-is-a-sample-site.com/shop/items and while the web page actually loads, the list of items for sale is stuck at loading. It seems to be related to Javascript. As a web developer, how do you diagnose this and the approach you would take to narrow down the problem?

1. You have learnt languages like C and C++ in university. It is said that languages like C and C++ is powerful, much more powerful than languages like Java, Python, Ruby, etc.. Is it fair to say that all softwares should be written in C and C++? Why and why not?

1. When developing a real-time strategy game on mobile, is it possible to use HTTP protocol?

# Part 2 - Blockchain Questions

If you do not have prior knowledge in blockchain and smart contracts, try your best to answer them. Or walk us through how you attempted to find the answers.

## Exploring NFT (Non Fungible Token)

Getting information for the NFT (Non Fungible Token) project, Bored Ape Yacht Club (BAYC)
You can explore details of the NFT at https://etherscan.io/address/0xbc4ca0eda7647a8ab7c2061c2e118a18a936f13d
Given the following example NFT (#3053) https://opensea.io/assets/0xbc4ca0eda7647a8ab7c2061c2e118a18a936f13d/3053

1. What is the metadata for NFT serial #3000 for BAYC? (Hint: The metadata is a json object explaining the attributes of the NFT)

1. How many NFTs minted in total under BAYC?

1. How many BAYC NFTs does the account 0x49c73c9d361c04769a452e85d343b41ac38e0ee4 hold?

1. Who is the owner of NFT serial #3000 for BAYC?

1. Is this metadata stored on IPFS or a Cloud Storage? What are the pros and cons?


# Part 3 - Simple Coding Assessment

[Build a Crypto Dashboard (Junior/Mid)](https://gitlab.com/coingecko-dev-test/interview-questions/-/blob/master/projects/crypto_dashboard_junior_mid.md)

---
# Submission instruction

1. Create a new repo
2. In your own git repo work on the Assessment.
3. Once you are done, share the link and access to your gitlab repo in the job application email.
4. Invite the users in https://gitlab.com/groups/coingecko-dev-test/-/group_members and grant Reporter role

Note: Do not create Merge Request against this repository, else your submission will be automatically disqualified.
