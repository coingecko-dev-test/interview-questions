# Software Engineer (University Graduate) Interview Questions
*Please include explanation along with your answers.*

1. In Java, the maximum size of an Array needs to be set upon initialization. Supposedly, we want something like an Array that is dynamic, such that we can add more items to it over time. Suggest how we can accomplish that (other than using ArrayList)?

1. Explain this block of code in Big-O notation.
    ```
    void sampleCode(int arr[], int size)
    {
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                printf("%d = %d\n", arr[i], arr[j]);
            }
         }
    }
    ```

1. You are a web developer working on a web application. Customer support approached you and escalated a problem that users are currently facing. The user visited the page at http://this-is-a-sample-site.com/shop/items and while the web page actually loads, the list of items for sale is stuck at loading. It seems to be related to Javascript. As a web developer, how do you diagnose this and the approach you would take to narrow down the problem?

1. You have learnt languages like C and C++ in university. It is said that languages like C and C++ is powerful, much more powerful than languages like Java, Python, Ruby, etc.. Is it fair to say that all softwares should be written in C and C++? Why and why not?


# Simple Coding Assessment

[Build a Crypto Dashboard (Junior/Mid)](https://gitlab.com/coingecko-dev-test/interview-questions/-/blob/master/projects/crypto_dashboard_junior_mid.md)

---
# Submission instruction

1. Create a new repo
2. In your own git repo work on the Assessment.
3. Once you are done, share the link and access to your gitlab repo in the job application email.
4. Invite the users in https://gitlab.com/groups/coingecko-dev-test/-/group_members and grant Reporter role

Note: Do not create Merge Request against this repository, else your submission will be automatically disqualified.
