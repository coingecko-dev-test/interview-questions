# DevOps Engineer Questions

# Part 1 - General

1. We are running CoinGecko.com on a cloud server. We are spinning up 10 processes and 10 threads to run the application. Do they mean the same thing?

# Part 2 - Cloud

1. I want to deploy a basic web application that is accessible over the internet with a SQL database to Amazon Web Service (AWS). Explain what services are needed and how to keep it secure.
If you are not familiar with AWS, you may suggest other service providers such as Google Cloud Platform, Microsoft Azure, or others and explain in your own words.

1. Serverless is gaining popularity such as AWS Lambda and GCP Cloud Functions. Now lets write our next app in serverless, and slowly re-write our existing app to serverless. Is that a good idea? Why, why not? What are the trade offs?

1. You are helping your company to host a web application on Amazon Web Services. As part of designing the architecture to host the application, you have the option to deploy the application in the following manner:-
(A) Single availability zone
(B) Multiple availability zones in a single region
(C) Multi regions
Describe which option you would choose and why? Also explain why you did not choose the other options.

# Part 3 - Docker

1. Explain what Docker is and why we need it.

# Part 4 - Database, DNS, and Ops

1. What is the trade-off between using SQL and NoSQL?
1. What is a CDN and why should you use it?
1. When to build, when to buy? Explain your thought process when deciding between using third-party tools or building your own tools for different DevOps operations.

