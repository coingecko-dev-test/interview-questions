# Senior Interview Questions

# Part 1 - General Software Questions

*Please include explanation along with your answers.*

1. Git: You are asked to work on a new existing project. When you browse the Git repo historical commits, you noticed many commits of small code changes and short description. Provide 3 advice you can give to the existing team to improve their Git practice?

1. You are a web developer and also in charge of managing servers. Suddenly we are getting complaints that users are not able to access any web pages. Is this a DDOS or something else? How do you diagnose this problem to be able to determine what is the likely root cause of this issue?

```
class SentimentVotesController < ApplicationController
  def create
    sentiment = if params[:sentiment] == "positive"
                  1
                elsif params[:sentiment] == "negative"
                  -1
                end

    ip_address = (request.env["HTTP_CF_CONNECTING_IP"] ||
                  request.env["Cf-Connecting-Ip"] ||
                  request.env["action_dispatch.remote_ip"]).to_s

    @sentiment_vote = SentimentVote.create(
      coin_id: params[:coin_id],
      sentiment: sentiment,
      ip_address: ip_address,
    )

    redirect_to :index

  end
end
```
1. We ran a poll to gather the sentiment of the market from our users. They can cast a vote whether the sentiment is positive or negative.
As this feature gets more popular, we are getting complaints from users that their page is stuck indefinitely after they tried to cast a vote.
Page response time has spiked and we have to find a way to fix this.
Suggest solutions and methods that we can use to overcome this.

1. You are helping your company to host a web application on Amazon Web Services. As part of designing the architecture to host the application, you have the option to deploy the application in the following manner:-
(A) Single availability zone
(B) Multiple availability zones in a single region
(C) Multi regions
Describe which option you would choose and why? Also explain why you did not choose the other options.

1. Visit coingecko.com and click the search bar you will see a Trending Search section. This basically shows the coins that are heavily searched within some time range to show what is trending.
As this feature becomes popular, we noticed people are gaming this by spamming search or giving incentive to people for making searches to improve a coin's tendency to hit trending.
What would you do to mitigate and counter this? Feel free to suggest any simple or sophisticated options we can take.

1. When developing a real-time strategy game on mobile, is it possible to use HTTP protocol?

# Part 2 - Blockchain Questions

If you do not have prior knowledge in blockchain and smart contracts, try your best to answer them. Or walk us through how you attempted to find the answers.

## Exploring NFT (Non Fungible Token)

Getting information for the NFT (Non Fungible Token) project, Bored Ape Yacht Club (BAYC)
You can explore details of the NFT at https://etherscan.io/address/0xbc4ca0eda7647a8ab7c2061c2e118a18a936f13d
Given the following example NFT (#3053) https://opensea.io/assets/0xbc4ca0eda7647a8ab7c2061c2e118a18a936f13d/3053

1. What is the metadata for NFT serial #3000 for BAYC? (Hint: The metadata is a json object explaining the attributes of the NFT)

1. How many NFTs minted in total under BAYC?

1. How many BAYC NFTs does the account 0x49c73c9d361c04769a452e85d343b41ac38e0ee4 hold?

1. Who is the owner of NFT serial #3000 for BAYC?

1. Is this metadata stored on IPFS or a Cloud Storage? What are the pros and cons?

# Part 3 - Simple Coding Assessment

[Build a Link Shortener](https://gitlab.com/coingecko-dev-test/interview-questions/-/blob/master/projects/link_shortener.md)

---
# Submission instruction

1. Create a new repo
2. In your own git repo work on the Assessment.
3. Once you are done, share the link and access to your gitlab repo in the job application email.
4. Invite the users in https://gitlab.com/groups/coingecko-dev-test/-/group_members and grant Reporter role

Note: Do not create Merge Request against this repository, else your submission will be automatically disqualified.
