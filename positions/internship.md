# Coingecko Engineering Internship - Written Assessment 

# Part 1 - General Software Questions

*Detail your answers with screenshots, pseudocode or references as needed*

1. You are a web developer of geddit - a website that displays a list of articles curated from the internet. You are receiving complaints from users about the site. When a user visits the page at the website, the list of **Popular Posts** is stuck at loading. The rest of the web page seems to be working as expected. 

![Loading](../../assets/loading.png)

What can possibly be an issue here? How would you identify the root cause?


# Part 2 - Blockchain Questions

*If you do not have prior knowledge in blockchain and smart contracts, try your best to answer them and walk us through how you attempted to find the answers.*

## Exploring NFT (Non Fungible Token)

You are tasked to gather some information on a popular NFT project, the [Bored Ape Yacht Club (BAYC)](https://opensea.io/collection/boredapeyachtclub).

1. What is the metadata for NFT serial #3000 for BAYC? (Hint: The metadata is a json object explaining the attributes of the NFT)

1. How many NFTs minted in total under BAYC?

1. How many BAYC NFTs does the account 0x49c73c9d361c04769a452e85d343b41ac38e0ee4 hold?

1. Who is the owner of NFT serial #3000 for BAYC?

1. Is this metadata stored on IPFS or a Cloud Storage? What are the difference, pros and cons?


The BAYC contract is accessible at: https://etherscan.io/address/0xbc4ca0eda7647a8ab7c2061c2e118a18a936f13d



---
# Part 3 - Simple Coding Assessment

[Build a Crypto Dashboard (Intern)](https://gitlab.com/coingecko-dev-test/interview-questions/-/blob/master/projects/crypto_dashboard_intern.md)

---
# Submission instruction

1. Create a new private repository on Gitlab.
2. Complete the Crypto Dashboard Assessment in the repository. Provide the working URL or application bundle (APK, IPA) in separate subdirectory `assets/`
3. Create a separate text or markdown file `written_assessment.md` and include your responses for Part 1 and Part 2 there. 
4. Invite the users in https://gitlab.com/groups/coingecko-dev-test/-/group_members and grant the group the **Reporter role**
5. Once you are done, return your repository URL via email. 

Note: Do not create Merge Request against this repository, else your submission will be automatically disqualified.
