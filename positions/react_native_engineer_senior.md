# React Native Senior Interview Questions
*Please include explanation along with your answers.*

1. Git: You are asked to work on a new existing project. When you browse the Git repo historical commits, you noticed many commits of small code changes and short description. Provide 3 advice you can give to the existing team to improve their Git practice?

1. You have just joined the company as a senior software engineer, on the first day on the job, the current mobile team shared with you some of the problems that they are facing :-
    - Our app quality is not good, we keep pushing hotfixes in various releases. This also slows down our iteration speed to ship new features.
    - For every hotfix that we need to push, we had to release a build. while the review process is in place, our app ratings continue to plummet.
    - Everytime before we deploy a release to iOS and Android, we had to manually compile, build, and sign the app for each platform before submitting to the store. 
    - As a software engineer, what suggestions do you have to help address these problems and come up with a plan to overcome this?

1. When reading an existing React Native codebase, you noticed that String variables are getting passed down to various classes and files finally leading to the View Component to be displayed. It also appears that this is how data is being presented in the codebase for the users. Is this approach alright or can you suggest ways to improve on this? Why?

# Coding Assessment

[Build a Crypto Shop with Cart](https://gitlab.com/coingecko-dev-test/interview-questions/-/blob/master/projects/crypto_cart.md)

---
# Submission instruction

1. Create a new repo
2. In your own git repo work on the Assessment.
3. Once you are done, share the link and access to your gitlab repo in the job application email.
4. Invite the users in https://gitlab.com/groups/coingecko-dev-test/-/group_members and grant Reporter role

Note: Do not create Merge Request against this repository, else your submission will be automatically disqualified.
