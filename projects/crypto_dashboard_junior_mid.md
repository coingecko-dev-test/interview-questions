# Build a Cryptocurrency Market dashboard showing market data + Shopping Cart to buy them

**dashboard page**
* User can see a page list of 20 or more coins.
* User can see the sparkline (7 days) in the page list.
* User can click into each coin to see more details of the coin.
* User can switch currency.

**cart/checkout**

* User can select coin to put into cart.
* User can enter the quantity of a coin for a given line item in the cart.
* User can checkout the order in cart.
* User can see total bill, order history, with the quantity, price per unit, total price.

**reference**

* [https://www.coingecko.com/en/api](https://www.coingecko.com/en/api)
* [https://api.coingecko.com/api/v3/coins/bitcoin?sparkline=true](https://api.coingecko.com/api/v3/coins/bitcoin?sparkline=true)
* [https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&sparkline=true](https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&sparkline=true)
* [https://api.coingecko.com/api/v3/exchange_rates](https://api.coingecko.com/api/v3/exchange_rates)

**bonus**

* Host this on a website with a public URL.
* Showcase usage of cloud services for (managed database, redis/memcache etc.)
* User can favorite a coin from page list and from coin page.
* User can toggle the page list to show only favorite coins.

---
### Submission
Follow the instructions as stated in the job position