# Build a Cryptocurrency Market dashboard showing market data.

Traders are always looking for a dashboard to monitor the overall cryptocurrency market and their favorite tokens.

Build a cryptocurrency market dashboard that displays a list of tokens and their market data. There are over 10,000 tokens out in the market and traders tend to also maintain a list of favorite coins.
Price of cryptocurrencies should also be displayed in various different currencies.


### Requirements:
1. using the API endpoints found in https://www.coingecko.com/en/api, build a cryptocurrency market dashboard page.
1. The page should at minimum display price, volume, name, symbol of the coin.
1. The page should show the graph of 7 days data, using the **sparkline** returned from api. For example sparkline data can be obtained using [https://api.coingecko.com/api/v3/coins/bitcoin?sparkline=true](https://api.coingecko.com/api/v3/coins/bitcoin?sparkline=true) or [https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&sparkline=true](https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&sparkline=true)
1. The page should allow user to "favorite" coins.
1. Handle pagination so users can page through more coins
1. build another "Favorites List" page, that displays only the list of favorited coins.
1. The page should allow user to switch currency. The price and volume should display the number in the currency selected. Use this endpoint to achieve the goal https://api.coingecko.com/api/v3/exchange_rates

---
### Submission
- If you decide to approach this as a web app, host the application so that we can access using a URL (eg. Heroku). If you decide to approach this as a mobile app, share with us the app package so we can run it.
- Our team will review your submission and schedule a video call with you to go through this solution.
- In the video call, describe at a high level how you approach the problem, why did you pick the coding solution, and explain the code.
- Emphasize on how you would explain to a team member on a new topic. The key is not so much about finding the "right answer", but sharing your thought process clearly to initiate a discussion.

